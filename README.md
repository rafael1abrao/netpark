# README #

Para rodar o projeto

## docker-compose up -d##

### Open api com swagger ###

### Product ###
#### localhost:3000/api-docs ####

### Product Sale ###
#### localhost:3001/api-docs ####

Conforme solicitado implementei o teste proposto. Utilizei SOLID como abordagem principal do projeto e existem 2 diferenças da proposta original. Utilizei Mysql ao invés do Postgres e Sequelize ao invés do Prisma.

O Volume do Mysql está sendo criado dentro da raiz do projeto. Acredito que nao de problema ao rodar no windows por exemplo.
Caso ocorra um erro, abra o docker-compose.yml e edite o volume do Mysql

## Algumas observações ##

As tabelas estāo sendo criadas automaticamente para ficar mais facil rodar o projeto e afim didatico. A quantidade do produto está dentro da tabela de produto (sei que está errado), mas foi apenas para nao ter que criar outra model. Acredito que vocês estarão analizando a arquitetura do projeto.

No cache implementei com Hset/set e hget/get. O set/get se mostrou mais rapido do que o hash, mesmo tendo q recriar a chave toda. Products esta com Hget/Hset e ProductSale com get/set

Faltou implementar o Kibana + ElasticCashe e Sentry
Em projetos para produçao normalmente utilizo essas 2 ferramentas.

Obrigado e aguardo feedback.

Rafael Abrao



