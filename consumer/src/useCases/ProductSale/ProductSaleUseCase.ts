/* eslint-disable no-useless-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-useless-constructor */
import ProductSale from '../../models/ProductSale'
import { IProductSaleRepository } from '../../repositories/IProductSaleRepository'

export class ProductSaleUseCase {
  constructor(
    private productSaleRepository: IProductSaleRepository,
  ) {
  }

  async find(): Promise<ProductSale[]> {
    return this.productSaleRepository.find()
  }

  async findOne(id: string): Promise<ProductSale> {
    const productSales = await this.productSaleRepository.findOne(id)
    return productSales
  }

  public async search(filter: string): Promise<{ rows: ProductSale[]; count: number }> {
    return await this.productSaleRepository.search(filter)
  }

  public async update(id: string, obj: ProductSale): Promise<ProductSale> {
    return await this.productSaleRepository.update(id, obj)
  }

  public async delete(id: string): Promise<void> {
    return await this.productSaleRepository.delete(id)
  }

  async create(obj: any): Promise<void> {
    try {
      const productSale: ProductSale = JSON.parse(obj)
      await this.productSaleRepository.create(productSale)
      console.log(productSale)
    } catch (e) {
      throw e
    }
  }
}
