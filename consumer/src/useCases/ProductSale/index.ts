import { ProductSaleUseCase } from '../ProductSale/ProductSaleUseCase'
import { ProductSaleRepository } from '../../repositories/ProductSaleRepository'
import { ProductSaleController } from '../../controllers/ProductSaleController'

const productSaleRepository = new ProductSaleRepository()
const productSaleUseCase = new ProductSaleUseCase(productSaleRepository)
const productSaleController = new ProductSaleController(productSaleUseCase)

export { productSaleUseCase, productSaleController }
