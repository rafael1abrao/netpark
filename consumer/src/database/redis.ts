/* eslint-disable @typescript-eslint/no-empty-function */
import Redis from 'ioredis'

class RedisInstance {
  private static instance: Redis.Redis

  private constructor() {}

  static getInstance(): Redis.Redis {
    if (!this.instance) {
      this.instance = new Redis({
        port: parseInt(process.env.REDIS_PORT),
        host: process.env.REDIS_HOST,
      })
      this.instance.on('connect', () => {
        console.log(`Redis - connected on ${process.env.REDIS_HOST}`)
      })
      this.instance.on('error', () => {
        console.log(`Unable to connect to the redis on ${process.env.REDIS_HOST}`)
        this.instance.disconnect()
        console.log('Redis disconnected')
        this.instance = null
        this.getInstance()
      })
    }
    return this.instance
  }
}
export default RedisInstance
