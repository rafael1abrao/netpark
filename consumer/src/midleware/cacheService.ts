import { Request, Response } from 'express'
import RedisInstance from '../database/redis'

class Cache {
  async cacheByProduct(req: Request, res: Response, next) {
    try {
      if (req.query.Id) {
        next()
      } else {
        const cacheData: any = await RedisInstance.getInstance().get('Products')
        if (cacheData) {
          res.json(await JSON.parse(cacheData))
        } else {
          next()
        }
      }
    } catch {
      next()
    }
  }
  async cacheByProductSale(req: Request, res: Response, next) {
    try {
      if (req.query.Id) {
        next()
      } else {
        const cacheData: any = await RedisInstance.getInstance().get('ProductSales')
        if (cacheData) {
          res.json(await JSON.parse(cacheData))
        } else {
          next()
        }
      }
    } catch {
      next()
    }
  }
}

export default new Cache()
