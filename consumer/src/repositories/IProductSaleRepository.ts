/* eslint-disable @typescript-eslint/no-explicit-any */
import ProductSale from '../models/ProductSale'

export interface IProductSaleRepository {
  find(): Promise<ProductSale[]>
  findOne(id: string): Promise<ProductSale>
  search(filter: string): Promise<{ rows: ProductSale[]; count: number }>
  create(obj: ProductSale): Promise<ProductSale>
  update(id: string, obj: ProductSale): Promise<ProductSale>
  delete(id: string): Promise<void>
}
