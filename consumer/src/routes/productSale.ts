import { Router } from 'express'
import { productSaleController } from '../useCases/ProductSale/index'
import cache from '../midleware/cacheService'

const productSaleRouter = Router()

productSaleRouter.get('/productSales', cache.cacheByProductSale, (request, response) => {
  return productSaleController.find(request, response)
})

productSaleRouter.get('/productSales/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productSaleController.findOne(request, response)
})

productSaleRouter.get('/products/:filter', (request, response) => {
  return productSaleController.search(request, response)
})

productSaleRouter.delete('/productSales/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productSaleController.delete(request, response)
})

productSaleRouter.put('/productSales/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productSaleController.update(request, response)
})


export { productSaleRouter }
