import { Router } from 'express'
import IndexController from '../controllers/IndexController'

const indexRouter = Router()

indexRouter.get('/', IndexController.index)

export { indexRouter }
