import { Router } from 'express'
import { indexRouter } from './index'
import { productSaleRouter } from './productSale'
import swaggerUi from 'swagger-ui-express'
import swaggerDocuments from '../../swagger.json'

const router = Router()
router.use('/', indexRouter)
router.use('/', productSaleRouter)
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocuments));

export { router }
