/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-useless-constructor */
import { Request, Response } from 'express'
import ProductSale from '../models/ProductSale'
import { ProductSaleUseCase } from '../useCases/ProductSale/ProductSaleUseCase'

export class ProductSaleController {
  constructor(private productSaleUseCase: ProductSaleUseCase) { }

  public async find(req: Request, res: Response): Promise<Response> {
    try {
      const productSales = await this.productSaleUseCase.find()
      return res.status(200).send(productSales)
    } catch {
      return res.status(500).send()
    }
  }

  public async findOne(req: Request, res: Response): Promise<Response> {
    try {
      const VariavelName = await this.productSaleUseCase.findOne(req.params.id)
      return res.status(200).send(VariavelName)
    } catch {
      return res.status(500).send()
    }
  }

  public async search(req: Request, res: Response): Promise<Response> {
    try {
      const filter: any = req.params.filter
      const product = await this.productSaleUseCase.search(filter)
      return res.send(product)
    } catch (error) {
      return res.status(500).send(error)
    }
  }

  public async create(req: Request, res: Response): Promise<Response> {
    const obj: ProductSale = req.body
    try {
      await this.productSaleUseCase.create(obj)
      return res.status(201).send()
    } catch (e) {
      return res.status(400).json({ message: e.message || 'Unexpected error.' })
    }
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    try {
      await this.productSaleUseCase.delete(req.params.id)
      return res.status(200).send()
    } catch {
      return res.status(400).send()
    }
  }

  public async update(req: Request, res: Response): Promise<Response> {
    try {
      const obj: ProductSale = req.body
      const VariavelName = await this.productSaleUseCase.update(req.params.id, obj)
      return res.status(200).send(VariavelName)
    } catch {
      return res.status(400).send()
    }
  }
}
