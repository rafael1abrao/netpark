import Sequelize, { Model } from 'sequelize'
import { Database } from '../database/mysql'

class Product extends Model {
  public id: string

  public name: string

  public description: string

  public quantity: number

  public readonly createdAt!: Date

  public readonly updatedAt?: Date

  public active!: boolean
}

Product.init(
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    name: Sequelize.STRING,
    description: Sequelize.STRING,
    quantity: Sequelize.INTEGER,
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    sequelize: Database.getConnection(),
    freezeTableName: true,
  },
)

export default Product
