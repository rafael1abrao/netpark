import Sequelize, { Model } from 'sequelize'
import { Database } from '../database/mysql'
import Product from './Product'

class ProductSale extends Model {
  public id: string

  public Product: Product

  public productId: string

  public quantity: number

  public value: number

  public readonly createdAt!: Date

  public readonly updatedAt?: Date

  public active!: boolean
}

ProductSale.init(
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
    },
    productId: Sequelize.STRING,
    product: Sequelize.VIRTUAL,
    quantity: Sequelize.INTEGER,
    value: Sequelize.DECIMAL(10, 2),
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    sequelize: Database.getConnection(),
    freezeTableName: true,
    timestamps: true,
  },
)

ProductSale.belongsTo(Product)

export default ProductSale
