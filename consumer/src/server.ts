import app from './app'

class Server {
  public port = parseInt(process.env.API_PORT) || 3000
  public app = app

  constructor() {
    this.app.set('port', this.port)
    this.app.listen(this.port, () => {
      console.log(`Api running on port ${this.port}`)
    })
  }
}

export default new Server()
