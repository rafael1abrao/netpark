import { Message } from 'amqplib';

export interface IRabbitProvider {
  publishInQueue(queue: string, message: string): Promise<any>;
  publishInExchange(
    exchange: string,
    routingKey: string,
    message: string
  ): Promise<boolean>;
  consume(queue: string, callback: (message: Message) => void): Promise<void>;
}
