import express from 'express'
import cors from 'cors'
import { router } from './routes/routes'
import { Database } from './database/mysql'
import { RabbitProvider } from './providers/RabbitProvider'
import { productSaleUseCase } from './useCases/ProductSale/index'
import morgan from 'morgan'
import RedisInstance from './database/redis'

class App {
  public express: express.Application

  constructor() {
    this.express = express()
    this.database()
    this.middlewares()
    this.routes()
    this.rabbitMQ()
  }

  private middlewares() {
    this.express.use(
      express.json({
        verify: (req: any, res, buf) => {
          req.rawBody = buf
        },
      }),
    )
    this.express.use(cors())
    this.express.options('*', cors())
    this.express.use(morgan('combined'))
  }

  private routes() {
    this.express.use(router)
  }

  private database() {
    Database.getConnection()
    Database.getInstance().isConnected()
    RedisInstance.getInstance()
  }

  private async rabbitMQ() {
    const queueProvider = new RabbitProvider()
    await queueProvider.init()
    await queueProvider.consume('ProductSale', (message) => productSaleUseCase.create(message.content));

  }
}

export default new App().express
