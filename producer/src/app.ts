import express from 'express'
import cors from 'cors'
import { router } from './routes/routes'
import { Database } from './database/mysql'
import RedisInstance from './database/redis'
import morgan from 'morgan'

class App {
  public express: express.Application

  constructor() {
    this.express = express()
    this.database()
    this.middlewares()
    this.routes()
  }

  private middlewares() {
    this.express.use(
      express.json({
        verify: (req: any, res, buf) => {
          req.rawBody = buf
        },
      }),
    )
    this.express.use(cors())
    this.express.options('*', cors())
    this.express.use(morgan('combined'))

  }

  private routes() {
    this.express.use(router)
  }

  private database() {
    Database.getConnection()
    Database.getInstance().isConnected()
    RedisInstance.getInstance()
  }
}

export default new App().express
