/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable no-useless-constructor */
import { QueryTypes, Sequelize } from 'sequelize'
import options from '../config/sequelize'

export class Database {
  private static _instance: Database
  private _connection: Sequelize

  private constructor() {}

  static getConnection(): Sequelize {
    return Database.getInstance()._connection
  }

  private connect() {
    this._connection = new Sequelize(options)
  }

  querytypes(): any {
    return QueryTypes
  }

  static getInstance(): Database {
    if (!Database._instance) {
      this._instance = new Database()
      this._instance.connect()
    }
    return this._instance
  }

  async isConnected(): Promise<void> {
    this._connection
      .authenticate()
      .then(function () {
        console.log(`Mysql - connected on ${options.host}`)
      })
      .catch(() => {
        console.error(`Unable to connect to the mysql on ${options.host}`)
        Database.getInstance().disconect()
        Database._instance = null
        Database.getInstance()
      })
  }

  async disconect(): Promise<void> {
    this._connection.close()
  }
}
