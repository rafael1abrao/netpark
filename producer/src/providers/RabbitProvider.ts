import { Connection, Channel, connect, Message } from 'amqplib';
import { IRabbitProvider } from './IRabbitProvider';

export class RabbitProvider implements IRabbitProvider {
  private static _instance: IRabbitProvider
  private conn: Connection
  private channel: Channel

  private constructor() {
    this.init();
  }

  async init(): Promise<void> {
    this.conn = await connect(process.env.RABBIT_URI);
    this.channel = await this.conn.createChannel();
    await this.channel.assertQueue(process.env.RABBIT_QUEUE, {durable: true});
    console.log('RabbitMQ - connected');
  }

  static getInstance(): IRabbitProvider {
    if (!RabbitProvider._instance) {
      this._instance = new RabbitProvider();
    }
    return this._instance
  }

  async publishInQueue(queue: string, message: string): Promise<any>{
    return this.channel.sendToQueue(queue, Buffer.from(message));
  }

  async publishInExchange(
    exchange: string,
    routingKey: string,
    message: string
  ): Promise<boolean> {
    return this.channel.publish(exchange, routingKey, Buffer.from(message));
  }

  async consume(queue: string, callback: (message: Message) => void): Promise<void>{
    return this.channel.consume(queue, (message) => {
      callback(message);
      this.channel.ack(message);
    });
  }
}