import moment from 'moment'

export class formatDate {
  public static formatToGMTDate(dateFrom: Date, gmt: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format('ddd, MMM D YYYY')
  }

  public static formatToGMTHour(dateFrom: Date, gmt: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format('HH:mm:ss')
  }

  public static formatToGMTHourAmPm(dateFrom: Date, gmt: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format('hh:mm:ss A')
  }

  public static formatToGMT(dateFrom: Date, gmt: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format('ddd, MMM D YYYY HH:mm:ss')
  }

  public static formatToShortUTC(dateFrom: Date, gmt: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format(`YYYY-MM-DDTHH:mm${gmt}`)
  }

  public static formatTo(dateFrom: Date, gmt: string, format: string): string {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).format(format)
  }

  public static toOffset(dateFrom: Date, gmt: string): Date {
    const dt = moment.parseZone(dateFrom.toUTCString())
    return dt.utcOffset(gmt).toDate()
  }

  public static getDates(startDate: moment.MomentInput, stopDate: moment.MomentInput): Array<Date> {
    const dateArray = []
    let currentDate = moment(startDate)
    while (currentDate.isSameOrBefore(moment(stopDate))) {
      dateArray.push(currentDate.toDate())
      currentDate = moment(currentDate, 'DD-MM-YYYY').add(1, 'days')
    }
    return dateArray
  }
}
