import { Request, Response } from 'express'
import Product from '../models/Product'
import { ProductRepository } from '../repositories/ProductRepository'

class ValidadeData {
  async productExist(req: Request, res: Response, next) {
    const rep = new ProductRepository()
    const product: Product = req.body
    const productAlreadyExist = await rep.search(product.name)
    if (productAlreadyExist.count > 0) {
      res.status(400).json('Product Already exists')
    } else {
      next()
    }
  }
}

export default new ValidadeData()
