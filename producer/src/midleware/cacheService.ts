import { Request, Response } from 'express'
import RedisInstance from '../database/redis'

class Cache {
  async cacheByProduct(req: Request, res: Response, next) {
    try {
      if (req.query.Id) {
        next()
      } else {
        const cacheData: any = await RedisInstance.getInstance().hgetall('Products')
        if (Object.keys(cacheData).length > 0) {
          console.log('Products in cache')
          res.json(await JSON.parse(cacheData))
        } else {
          next()
        }
      }
    } catch {
      next()
    }
  }
  async cacheByProductSale(req: Request, res: Response, next) {
    try {
      if (req.query.Id) {
        next()
      } else {
        const cacheData: any = await RedisInstance.getInstance().get('ProductsSale')
        if (cacheData) {
          res.json(await JSON.parse(cacheData))
        } else {
          next()
        }
      }
    } catch {
      next()
    }
  }
}

export default new Cache()
