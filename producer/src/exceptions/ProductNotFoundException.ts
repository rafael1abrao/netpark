import BaseException from './BaseException'

export default class ProductNotFoundException extends BaseException {
  constructor() {
    super(
      'product.exception.notfound',
      'The Given product name was not found or it\'s inactive',
      400, // Bad Request
    )
  }
}
