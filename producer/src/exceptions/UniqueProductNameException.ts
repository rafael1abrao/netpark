import BaseException from './BaseException'

export default class UniqueUsernameException extends BaseException {
  constructor() {
    super(
      'product.exception.name.unique',
      'The given product name is not available',
      409, // Conflit
    )
  }
}
