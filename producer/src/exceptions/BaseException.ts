export default abstract class BaseException extends Error {
  public key!: string
  public description!: string
  public statusCode!: number
  public details?: any

  constructor(key: string, message: string, statusCode = 500, details = null) {
    super(message)
    this.key = key
    this.description = message
    this.statusCode = statusCode
    this.details = details
  }
}
