/* eslint-disable @typescript-eslint/no-explicit-any */
import Product from '../models/Product'
import { IBaseRepository } from './IBaseRepository'
import { Op } from 'sequelize'
import _ from 'lodash'

export class ProductRepository implements IBaseRepository<Product> {

  async find(): Promise<Product[]> {
    return await Product.findAll({ include: { all: true } })
  }

  async findOne(id: string): Promise<Product> {
    return Product.findByPk(id)
  }

  async search(filter: string): Promise<{ rows: Product[]; count: number }> {
    const value = {
      [Op.like]: "%" + filter + "%"
    }

    const fields = Object.keys(
      _.omit(Product.rawAttributes, [
        "id",
        "createdAt",
        "updatedAt",
      ])
    );

    const filters = {};
    fields.forEach((item) => (filters[item] = value));

    const result = await Product.findAndCountAll({
      where: {
        [Op.or]: filters
      }
    });

    return result
  }

  async create(product: any): Promise<Product> {
    try {
      const result = await Product.create(product)

      return result
    } catch (e) {
      throw e
    }
  }

  async delete(id: string): Promise<void> {
    try {
      await Product.destroy({ where: { id: id } })
    } catch (e) {
      throw e
    }
  }

  async update(id: string, product: Product): Promise<Product> {
    try {
      await Product.update(product, { where: { id: id } })
      return await this.findOne(id)
    } catch (e) {
      throw e
    }
  }
}
