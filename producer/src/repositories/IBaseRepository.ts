import { Model } from "sequelize";

// that class only can be extended
export interface IBaseRepository<M extends Model> {
  create(item: M): Promise<M> 
  update(id: string, item: M): Promise<M> 
  delete(id: string): Promise<void> 
  find(): Promise<M[]> 
  findOne(id: string): Promise<M>
  search(filter: string): Promise<{ rows: M[]; count: number }>
}
