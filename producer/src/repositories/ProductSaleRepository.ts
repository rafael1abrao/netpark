/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import ProductSale from '../models/ProductSale'
import { IBaseRepository } from './IBaseRepository'
import { Op } from 'sequelize'
import RedisInstance from '../database/redis'
import _ from 'lodash'

export class ProductSaleRepository implements IBaseRepository<ProductSale> {

  async find(): Promise<ProductSale[]> {
    const productSale = ProductSale.findAll({ include: { all: true } })
    RedisInstance.getInstance().set('ProductSales', JSON.stringify(productSale));
    RedisInstance.getInstance().expire('ProductSales', 60);
    return productSale
  }

  async findOne(id: string): Promise<ProductSale> {
    const productSales = ProductSale.findByPk(id, { include: { all: true } })
    return productSales
  }

  async search(filter: string): Promise<{ rows: ProductSale[]; count: number }> {
    const value = {
      [Op.like]: "%" + filter + "%"
    }

    const fields = Object.keys(
      _.omit(ProductSale.rawAttributes, [
        "id",
        "createdAt",
        "updatedAt",
      ])
    );

    const filters = {};
    fields.forEach((item) => (filters[item] = value));

    const result = await ProductSale.findAndCountAll({
      where: {
        [Op.or]: filters
      }
    });

    return result
  }

  async create(obj: any): Promise<ProductSale> {
    try {
      const result = await ProductSale.create(obj)
      await RedisInstance.getInstance().flushall()
      return result
    } catch (e) {
      throw e
    }
  }

  async delete(id: string): Promise<void> {
    // const transaction = await Database.getConnection().transaction()
    try {
      await ProductSale.destroy({ where: { id: id } })
      await RedisInstance.getInstance().flushall()
      // transaction.commit()
    } catch (e) {
      // transaction.rollback()
      throw e
    }
  }

  async update(id: string, obj: ProductSale): Promise<ProductSale> {
    try {
      await ProductSale.update(obj, { where: { id: id } })
      await RedisInstance.getInstance().flushall()
      return await this.findOne(id)
    } catch (e) {
      throw e
    }
  }
}
