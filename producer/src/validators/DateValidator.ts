export class DateValidator {
  public static isRangeValid(dateFrom: Date, dateTo: Date): boolean {
    return dateFrom.getTime() < dateTo.getTime()
  }
}
