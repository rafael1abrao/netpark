import { Router } from 'express'
import { productController } from '../useCases/Product/index'
import validadeData from '../midleware/validateDataService'
import cache from '../midleware/cacheService'

const productRouter = Router()

productRouter.get('/products', cache.cacheByProduct, (request, response) => {
  return productController.index(request, response)
})

productRouter.get('/products/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productController.get(request, response)
})

productRouter.get('/products/:search', (request, response) => {
  return productController.search(request, response)
})

productRouter.post(
  '/products',
  validadeData.productExist,
  (request, response) => {
    return productController.create(request, response)
  },
)

productRouter.delete('/products/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productController.delete(request, response)
})

productRouter.put('/products/:id([-a-zA-Z0-9]{36})', (request, response) => {
  return productController.update(request, response)
})

export { productRouter }


