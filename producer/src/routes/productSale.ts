import { Router } from 'express'
import { productSaleController } from '../useCases/ProductSale/index'
import cache from '../midleware/cacheService'

const productSaleRouter = Router()

productSaleRouter.post('/productSales', (request, response) => {
  return productSaleController.create(request, response)
})

export { productSaleRouter }
