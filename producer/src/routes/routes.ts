import { Router } from 'express'
import { productRouter } from './product'
import { productSaleRouter } from './productSale'
import { indexRouter } from './index'
import swaggerUi from 'swagger-ui-express'
import swaggerDocuments from '../../swagger.json'

const router = Router()

router.use('/', indexRouter)
router.use('/', productRouter)
router.use('/', productSaleRouter)
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocuments));

export { router }
