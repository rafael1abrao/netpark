/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-useless-constructor */
import { Request, Response } from 'express'
import ProductSale from '../models/ProductSale'
import { ProductSaleUseCase } from '../useCases/ProductSale/ProductSaleUseCase'

export class ProductSaleController {
  constructor(private productSaleUseCase: ProductSaleUseCase) { }

  public async create(req: Request, res: Response): Promise<Response> {
    const obj: ProductSale = req.body
    try {
      await this.productSaleUseCase.create(obj)
      return res.status(201).send()
    } catch (e) {
      return res.status(400).json({ message: e.message || 'Unexpected error.' })
    }
  }
}
