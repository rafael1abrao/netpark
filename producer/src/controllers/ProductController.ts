/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-useless-constructor */
import { Request, Response } from 'express'
import Product from '../models/Product'
import { ProductUseCase } from '../useCases/Product/ProductUseCase'
import BaseException from '../exceptions/BaseException'

export class ProductController {
  constructor(private productUseCase: ProductUseCase) { }

  public async index(req: Request, res: Response): Promise<Response> {
    try {
      const product = await this.productUseCase.index()
      return res.status(200).send(product)
    } catch {
      return res.status(500).send()
    }
  }

  public async get(req: Request, res: Response): Promise<Response> {
    try {
      const product = await this.productUseCase.get(req.params.id)
      return res.status(200).send(product)
    } catch {
      return res.status(500).send()
    }
  }

  public async create(req: Request, res: Response): Promise<Response> {
    const obj: Product = req.body
    try {
      const product: Product = await this.productUseCase.create(obj)
      return res.status(201).send(product)
    } catch (e) {
      return res.status(400).json({ message: e.message || 'Unexpected error.' })
    }
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    try {
      await this.productUseCase.delete(req.params.id)
      return res.status(200).send()
    } catch {
      return res.status(400).send()
    }
  }

  public async update(req: Request, res: Response): Promise<Response> {
    try {
      const obj: Product = req.body
      const product = await this.productUseCase.update(req.params.id, obj)
      return res.status(200).send(product)
    } catch (error) {
      if (error instanceof BaseException) {
        return res.status(error.statusCode).send(error)
      }
      return res.status(400).send()
    }
  }

  public async search(req: Request, res: Response): Promise<Response> {
    try {
      const filter: any = req.params.search
      const product = await this.productUseCase.search(filter)
      return res.send(product)
    } catch (error) {
      return res.status(500).send(error)
    }
  }
}
