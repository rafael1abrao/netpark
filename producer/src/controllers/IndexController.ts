import { Request, Response } from 'express'

class IndexController {
  public async index(req: Request, res: Response): Promise<Response> {
    return res.json({
      title: 'NetPark API',
      version: '0.0.1',
    })
  }
}

export default new IndexController()
