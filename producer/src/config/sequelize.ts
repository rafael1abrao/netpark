import { Options } from 'sequelize'
require('dotenv/config')

function getOptions(): Options {
  return {
    dialect: 'mysql',
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    logging: process.env.DB_LOGGING === 'true' ? console.log : false,
    port: Number.parseInt(process.env.DB_PORT),
    pool: {
      max: 64,
      min: 16,
      idle: 15000,
    },
    define: {
      timestamps: true,
    },
  }
}

export default getOptions()
