/* eslint-disable no-useless-constructor */
import Product from '../../models/Product'
import { IBaseRepository } from '../../repositories/IBaseRepository'
import { IRabbitProvider } from '../../providers/IRabbitProvider'
import RedisInstance from '../../database/redis';
// import UniqueUsernameException from '../../exceptions/UniqueUsernameException'
// import EmailAlreadyExistsException from '../../exceptions/EmailAlreadyExistsException'

export class ProductUseCase {
  constructor(
    private productRepository: IBaseRepository<Product>,
    private queueProvider: IRabbitProvider,
  ) { }

  public async index(): Promise<Product[]> {
    const products = await this.productRepository.find()

    products.forEach(p => {
      RedisInstance.getInstance().hset('Products', p.id, JSON.stringify(p));
    });
    RedisInstance.getInstance().expire('Products', 600);

    return products;

  }

  public async get(id: string): Promise<Product> {
    return await this.productRepository.findOne(id)
  }

  public async search(filter: string): Promise<{ rows: Product[]; count: number }> {
    return await this.productRepository.search(filter)
  }

  async create(obj: Product): Promise<Product> {

    // put some validations for example.
    obj.name = obj.name.toLowerCase()

    if (!obj.description) {
      throw new Error('product.creation.description.required')
    }

    const product: Product = await this.productRepository.create(obj)

    RedisInstance.getInstance().hset('Products', product.id, JSON.stringify(product))

    return product
  }

  public async delete(id: string): Promise<void> {
    await this.productRepository.delete(id)
    RedisInstance.getInstance().hdel('Products', id)
  }

  public async update(id: string, obj: Product): Promise<Product> {
    const foundProduct = await this.productRepository.findOne(id)

    if (!foundProduct) throw new Error('product.notfound')

    const p = await this.productRepository.update(id, obj)

    RedisInstance.getInstance().hset('Products', p.id, JSON.stringify(p))

    return p
  }
}
