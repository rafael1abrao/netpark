
import { RabbitProvider } from '../../providers/RabbitProvider'
import { ProductController } from '../../controllers/ProductController'
import { ProductRepository } from '../../repositories/ProductRepository'
import { ProductUseCase } from './ProductUseCase'

const queueProvider = RabbitProvider.getInstance()
const productRepository = new ProductRepository()
const productUseCase = new ProductUseCase(productRepository, queueProvider)
const productController = new ProductController(productUseCase)

export { productUseCase, productController }
