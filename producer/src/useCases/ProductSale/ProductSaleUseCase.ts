/* eslint-disable no-useless-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable no-useless-constructor */
import ProductSale from '../../models/ProductSale'
import Product from '../../models/Product'
import { IBaseRepository as ProductSaleRepository } from '../../repositories/IBaseRepository'
import { IBaseRepository as ProductRepository } from '../../repositories/IBaseRepository'
import { IRabbitProvider } from '../../providers/IRabbitProvider'


export class ProductSaleUseCase {
  constructor(
    private productSaleRepository: ProductSaleRepository<ProductSale>,
    private productRepository: ProductRepository<Product>,
    private queueProvider: IRabbitProvider,
  ) { }

  async create(obj: ProductSale): Promise<void> {
    try {
      const product = await this.productRepository.findOne(obj.productId)

      if (product && obj.quantity <= product.quantity) {
        obj.Product = product
        product.quantity -= obj.quantity
        await this.productRepository.update(product.id, product.get())
        await this.queueProvider.publishInQueue('ProductSale', JSON.stringify(obj));
      } else {
        throw new Error('product.notfound')
      }
    } catch (e) {
      throw e
    }
  }
}
