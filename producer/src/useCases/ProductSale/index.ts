import { RabbitProvider } from '../../providers/RabbitProvider'
import { ProductSaleUseCase } from '../ProductSale/ProductSaleUseCase'
import { ProductSaleRepository } from '../../repositories/ProductSaleRepository'
import { ProductRepository } from '../../repositories/ProductRepository'
import { ProductSaleController } from '../../controllers/ProductSaleController'

const queueProvider = RabbitProvider.getInstance()
const productSaleRepository = new ProductSaleRepository()
const productRepository = new ProductRepository()
const productSaleUseCase = new ProductSaleUseCase(productSaleRepository, productRepository, queueProvider)
const productSaleController = new ProductSaleController(productSaleUseCase)

export { productSaleUseCase, productSaleController }
